document.addEventListener('DOMContentLoaded', function() {

  

 let setApp = function() {
  
    let h2ContainerDiv = document.querySelector('#h2-container-div');
    

    let marketingAndCom = 'ASSISTANT MARKETING & COMMUNICATION';
    
    // Fonction permettant de placer chaque lettre d'une phrase dans une span et lui attribut un ID
    function addDiv( sentence, domElement, optionRotate = false, parentType) {
    
         let array = sentence.split('');
    
           let i = 0;
    
             for( let letter of array ) {
    
                i++;
    
               let div = document.createElement('div');
                   div.setAttribute('id', 'div-' + parentType + '-' + i);
                
                   div.innerHTML = letter;
    
                  domElement.appendChild(div);
    
                  if(optionRotate === true ) {
                      div.className = 'div-hide';
                  }
    
                }
        
    }
    
    
    // Fonction permettant de faire pivoter les lettres de la chaîne les unes après les autres 
    // afin de les monter
    
    function showAnimation() {
       
       let counter = 0;
       
       if(counter < 35) {
         
         // Active l'animation avec le nombre de secondes de retard indiqué
         setTimeout(function() {

          let timeAnimation = setInterval(function() {
             
             if(counter >= 35) {
                
                clearInterval(timeAnimation);
               }
               else {

                  counter++; 
                  
                  let divShow = document.querySelector('#div-h2-' + counter);
                  divShow.className = 'div-show';
               }
                  
            }, 60);  
         
         }, 3000);
            
         }
       
      }



      
               let titleName = 'ALEXIS SERVET';
               let h1ContainerDiv = document.querySelector('#h1-container-div');

      // Fonction permettant d'injecter chaque lettre d'un mot dans une 'div'
      // et de la rattacher à un container.

      function setTitle(word, domContainer) {

         wordSplit = word.split('');
         
         let iteration = 0;
    
         for( let wordLetter of wordSplit ) {

            iteration++;

           let div = document.createElement('div');
               div.setAttribute('id', 'div-title-' + iteration);

               div.className = 'invisible';

               div.innerHTML = wordLetter;
               
              domContainer.appendChild(div);

   

            }


      }


      // *****  Animation du header
      function headerAnimation() {

         let cameleon = document.querySelector('#cameleon');

             let iCounter = 0;
            
             setTimeout(function() {
              
         

           let headerTitleInterval = setInterval(function() {
 
                  if(iCounter < 13) {
                     iCounter++;

                       let headerDivShow = document.querySelector('#div-title-' + iCounter);
                           headerDivShow.className = 'appears';
                       

                  } 
                  else {
                     clearInterval(headerTitleInterval);
                  }

             }, 90);

            }, 1500);

             
                    

             setTimeout(function() {
                cameleon.className = 'visible';
                
             }, 500);


      }

      // On pose le titre de la page dans le DOM
      setTitle(titleName, h1ContainerDiv);
      // On active l'animation d'apparition du header dans le DOM
      headerAnimation();
      // On crée la phrase dans le dom
      
      addDiv( marketingAndCom, h2ContainerDiv, true, 'h2' );

      // On active l'animation sur le premier mot du h2
      showAnimation();

     



 // Ferumeture de setApp()
 }

   
   setApp();

   
//  Fermerue de l'écouteur DOMContentLoaded 
});