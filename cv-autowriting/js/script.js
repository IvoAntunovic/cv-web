document.addEventListener('DOMContentLoaded', function() {
   
   
      
// Fonction qui facilite l'utilisation de query selector
   function html(cssSelector) {
           return document.querySelector(cssSelector);
   }

/*
 ****************************************
 * ||| ANIMATION POUR LE HEADER DU CV |||
 ****************************************
*/



/*
 ****************************************
 * |||| ANIMATION POUR LE MAIN DU CV ||||
 ****************************************
 */
   // Header des paragraphes (Expérience, Compétences...)  
   let domExperienceHeader;
   let domSkillsHeader;
   // Paragraphe experience dans le DOM
   let domExperienceParagraph;
   let domSkillsParagraph;
   
   // Text à rattacher aux paragraphe (expérience, compétences...)
   let experienceText;
   let domSkillsText = 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit accusamus dolorum, assumenda aliquam tempora et, perferendis porro harum nisi facere atque, dicta nonvoluptas! Quo, eligendi numquam. Veritatis, quam nobis!';
   
   // Gouttes d'encres des paragraphes (Exmperience, compétence...)
   let domExperienceInkDrop;
   let domSkillsInkdrop;
   
   // Flag pour savoir si l'élément a déjà été cliqué
   let flagExperience = false;
   let flagSkills = false;

   


/*
* 
* Fonction permettant d'animer
* l'écriture des paragrpahes dans le DOM 
*/
function slowWriting( sentence, domElement, writingSpeed) {
   
   let letters = sentence.split('');
   
   let i = 0;
   
   setInterval(function() {
      
      if(i <= letters.length - 1) {
         
         domElement.innerHTML += letters[i];
         
         i++;
         
      }
      
   }, writingSpeed);
   
}


/**
 * Fonction permettant d'activer l'animation
 * avec la goute d'encre qui tombe 
 */

function dropAnimation(dropElement) {

   dropElement.src = "./img/ink-drop.png";

   dropElement.style.width = '150px';

   dropElement.className = 'ink-stain';

}


                  domExperienceHeader = html('#experience-header');
                  
                  domExperienceHeader.addEventListener('click', function() {


                     if(flagExperience === false) {

                        domExperienceInkDrop = html('#experience-black-drop');
      
                        dropAnimation(domExperienceInkDrop);
           
                        domExperienceParagraph = html('#experience-paragraph');
      
                        experienceText = 'Lorem ipsum is, quam nobis!';
      
                        slowWriting(experienceText, domExperienceParagraph, 20);

                        flagExperience = true;
                        
                     }
                     else {

                        return;

                     }

         
              });
   


// FERMETURE DU DOMContentLoaded
})