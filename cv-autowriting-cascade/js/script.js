document.addEventListener('DOMContentLoaded', function() {
    
   
   // Fonction qui facilite l'utilisation de query selector
   function html(cssSelector) {
      return document.querySelector(cssSelector);
   }
   
   


   // Variables pour la partie "HEADER"
   let h1 = html('#h1'),
      h1_content = 'Ivo Antunovic';
   
   let h2 = html('#h2'),
       h2_content = '< DÉVELOPPEUR WEB FULL STACK / >';
   
          // Images du header
          let inkDrop = html('.ink-drop-container');
          let blackCloud = html('#black-cloud');

 


   // Variables pour la partie "Réalisation/web"
   let achievements_h3 = html('#achievements-h3'),
       achievements_h3_content = 'Réalisations/web';

   let achievements_h4_1 = html('#achievements-h4-1'),
       achievements_h4_content_1 = 'CV — CV Web Responsive';

   let achievements_p_1 = html('#achievements-p-1'),
       achievements_p_content_1 = 'Réalisation d’un CV sous la forme d’un site web statique, monopage, adapté à plusieurs supports: mobile, tablette et format bureau .';


   let achievements_h4_2 = html('#achievements-h4-2'),
       achievements_h4_content_2 = 'Map Events — Application d’événements cartographiés';

   let achievements_p_2 = html('#achievements-p-2'),
       achievements_p_content_2 = 'Réalisation d’une application permettant de créer des événements  culturels, cartographiés via un service web . ';
   
       
   // Variables pour la partie "Expérience"
   




       /*
       * 
       * Fonction permettant d'animer
       * l'écriture des paragrpahes dans le DOM 
       */
      function autoWriting( sentence, domElement, writingSpeed, animationTiming) {
         
         let letters = sentence.split('');
         
         let i = 0;
         
         setTimeout(function() {
            setInterval(function() {
               
               if(i <= letters.length - 1) {
                  
                  domElement.innerHTML += letters[i];
                  
                  i++;
                  
               }
               
            }, writingSpeed);
         }, animationTiming);
         
      }
      
      
      // Animation faisant apparaître le prénom et le nom dans le DOM
      function inkDropAnimation(animationTiming) {
           setTimeout(function() {
                   inkDrop.className = 'none';     
                   blackCloud.classList.remove('none');
      
                   setTimeout(function() {
                      h1.classList.replace('invisible', 'visible');
                   }, 500);
      
      
      
           }, animationTiming);
      }
      
/*
****************************************
* ||| ANIMATION POUR LE HEADER DU CV |||
****************************************
*/

function headerAnimation(){

   inkDropAnimation(1300);
   autoWriting(h2_content, h2, 70, 3400);

}

/*
 ****************************************
 * |||| ANIMATION POUR LE MAIN DU CV ||||
 ****************************************
 */
function mainAnimation()
{
   autoWriting(achievements_h3_content, achievements_h3, 10, 6000);

   autoWriting(achievements_h4_content_1, achievements_h4_1, 10, 6000);
   autoWriting(achievements_p_content_1, achievements_p_1, 10, 8000);

   autoWriting(achievements_h4_content_2, achievements_h4_2, 10, 6000);
   autoWriting(achievements_p_content_2, achievements_p_2, 10, 8000);


}






// On active l'animation dans le header 
headerAnimation();
// On active l'animation dans le main
mainAnimation();


// FERMETURE DU DOMContentLoaded
})